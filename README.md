# CMRDD (CM Rounded)

The _CMRDD_ typeface is a rounded variation of the _Computer Modern_. Originally designed by Donald E. Knuth using Metafont, the [original Metafont source files](https://ctan.org/pkg/cm-mf?lang=en) of _Computer Modern_ were modified by Paul Bernhard to render _CMRDD_. Next to minor modifications to individual characters and some added glyphs, the main work consists in the adjustment of _CM's_ 72 parameters.

_CMRDD_ is licensed under _SIL Open Font License v1.1 (OFL-1.1)_, therefor free for use, modification and distribution.

![CMRDD Specimen](images/cmrdd_specimen_01.png)

## Outline versions (TL;DR)

The four outline versions of CMRDD are individually available as .ttf, .woff and .woff2 in the [/outline](/outline) folder or as compressed archives [cmrdd-family.tar.gz](outline/cmrdd-family.tar.gz) / [cmrdd-family.zip](outline/cmrdd-family.zip).

For any requests txt <mailto:mail@pbernhard.com>.

## Rendering from source

All necessary Metafont source files of CMRDD are available in the [/src](/src) folder of this respository.

You will need a basic understanding of Metafont in order to render new font files and convert them to outline fonts. The structure of CMRDD source files follows the common architecture of Metafont files, starting from a basic file to set up the font parameters which is then applied to all character definitions.

CMRDD [/src](/src) has one shared parameter file `cmrdd.mf` which has all the original parameters of Knuth's _Computer Modern_. For each cut there is one file `cmrddr12.mf` (Regular), `cmrddi12.mf` (Italic), … which sets the weight and slant.

You can use [SWRS' mfbuild](https://gitlab.com/swrs/mfbuild) script collection to facilitate rendering and conversion of Metafont files.

## ToDo
- write docs on rendering and converting Metafont files
- test CMRDD in the wild
- ligatures of original CM

## Version

0.9.1
