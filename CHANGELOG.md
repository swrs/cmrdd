## 0.9.1
- changed shape of _g_
- increased individual letter spacings
- changed slant angle of _Italic_ cut
- added copyright text to outline versions

## 0.9.0
- First published version
- Rendered cuts Regular, Italic, Bold and ExtraBold
- Added License
