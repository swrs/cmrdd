% CMRDD BASE
% This file contains the base parameters which are shared among all CMRDD family members. As the CMRDD is based on the Computer Modern by Donald E. Knuth almost all parameters stem from the original CM source files.

% computer modern base file
if unknown cmbase: input cmbase fi

% font identifier and size
font_identifier:="CMOL"; font_size 12pt#;

u#:=28/36pt#;      % unit width
width_adj#:=0pt#;    % width adjustment for certain characters
serif_fit#:=0pt#;    % extra sidebar near lowercase serifs
cap_serif_fit#:=0pt#;    % extra sidebar near uppercase serifs
letter_fit#:=-2/36pt#;    % extra space added to all sidebars

body_height#:=300/36pt#;  % height of tallest characters
asc_height#:=300/36pt#;    % height of lowercase ascenders
cap_height#:=300/36pt#;    % height of caps
fig_height#:=300/36pt#;    % height of numerals
x_height#:=235/36pt#;    % height of lowercase without ascenders
math_axis#:=132/36pt#;    % axis of symmetry for math symbols
bar_height#:=130/36pt#;    % height of crossbar in lowercase e
comma_depth#:=60/36pt#;    % depth of comma below baseline
desc_depth#:=96/36pt#;    % depth of lowercase descenders

crisp#:=weight#;    % diameter of serif corners
tiny#:=weight#;    % diameter of rounded corners
fine#:=weight#;    % diameter of sharply rounded corners
thin_join#:=weight#;    % width of extrafine details
hair#:=weight#;    % lowercase hairline breadth (set by weight variable)
stem#:=weight#;    % lowercase stem breadth (set by weight variable)
curve#:=weight#;    % lowercase curve breadth (set by weight variable)
ess#:=.85 * weight#;      % breadth in middle of lowercase s
flare#:=weight#;    % diameter of bulbs or breadth of terminals
dot_size#:=1.1 * weight#;    % diameter of dots
cap_hair#:=weight#;    % uppercase hairline breadth (set by weight variable)
cap_stem#:=weight#;    % uppercase stem breadth (set by weight variable)
cap_curve#:=weight#;    % uppercase curve breadth (set by weight variable)
cap_ess#:=weight#;    % breadth in middle of uppercase s (set by weight variable)
rule_thickness#:=weight#;  % thickness of lines in math symbols (set by weight variable)

dish#:=0pt#;      % amount erased at top or bottom of serifs
bracket#:=0pt#;      % vertical distance from serif base to tangent
jut#:=39/36pt#;      % protrusion of lowercase serifs
cap_jut#:=39/36pt#;    % protrusion of uppercase serifs
beak_jut#:=0pt#;    % horizontal protrusion of beak serifs
beak#:=0pt#;    % vertical protrusion of beak serifs
vair#:=25/36pt#;    % vertical diameter of hairlines
notch_cut#:=.5 * weight#;    % maximum breadth above or below notches
bar#:=.9 * weight#;      % lowercase bar thickness
slab#:=25/36pt#;    % serif and arm thickness
cap_bar#:=.9 * weight#;    % uppercase bar thickness
cap_band#:=.9 * weight#;    % uppercase thickness above/below lobes
cap_notch_cut#:=.5 * weight#;  % max breadth above/below uppercase notches
serif_drop#:=0pt#;    % vertical drop of sloped serifs
stem_corr#:=0pt#;    % for small refinements of stem breadth
vair_corr#:=0pt#;    % for small refinements of hairline height
apex_corr#:=12/36pt#;    % extra width at diagonal junctions

o#:=5/36pt#;      % amount of overshoot for curves
apex_o#:=4/36pt#;    % amount of overshoot for diagonal junctions

fudge:=1;      % factor applied to weights of heavy characters
math_spread:=-1;    % extra openness of math symbols
superness:=1.05 * (1/sqrt2);    % parameter for superellipses
superpull:=0;      % extra openness inside bowls
beak_darkness:=0;    % fraction of triangle inside beak serifs
ligs:=0;      % level of ligatures to be included

square_dots:=false;    % should dots be square?
hefty:=true;      % should we try hard not to be overweight?
serifs:=false;      % should serifs and bulbs be attached?
monospace:=false;    % should all characters have the same width?
variant_g:=true;    % should an italic-style g be used?
low_asterisk:=true;    % should the asterisk be centered at the axis?
math_fitting:=false;    % should math-mode spacing be used?

slant:=italic; % set slant with italic variable

% call the CMRDD driver
generate croman;
